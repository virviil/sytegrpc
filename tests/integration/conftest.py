import pytest

from pathlib import Path
import getpass

from vflows.models_loader import load_models


@pytest.fixture
def models_index():
    return load_models(
        Path(f"/Users/{getpass.getuser()}/syte/deeptags/vmr_network_data/models")
    )


@pytest.fixture
def base_host():
    return "vmr-network-app-{}.staging.syte.app:443"
