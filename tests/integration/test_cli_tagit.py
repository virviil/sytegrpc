import json
import orjson
import pytest


from vflows.context_manager import VFlowsContextManager
from vflows.requirements import Requirements

from sytedt.inferer import Input, InputEntity

from sytegrpc.client_factory import GrpcClientFactory


@pytest.mark.asyncio
async def test_tagit_client(models_index, base_host):
    requirements = Requirements(GrpcClientFactory(base_host))
    async with VFlowsContextManager(models_index, requirements) as _cm:
        tagit_client = requirements.clients["220201-tagit"]
        item_data = json.load(open("tests/resources/item.json", "r"))
        data_to_model = {
            "products": [
                {
                    "categories": item_data["categories"],
                    "sku": item_data["sku"],
                    "title": item_data["title"],
                }
            ]
        }
        prediction = await tagit_client.predict(
            Input(entities=[InputEntity(data=orjson.dumps(data_to_model))])
        )
        result = json.loads(prediction.entities[0].data)
        assert result["gender"] == "female"
