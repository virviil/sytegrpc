import json
import pytest
import numpy as np

from sytedt.inferer import Input, InputEntity

from vflows.context_manager import VFlowsContextManager
from vflows.requirements import Requirements
from vflows.dt_converter import DtConverter

from sytegrpc.client_factory import GrpcClientFactory


@pytest.mark.asyncio
async def test_tagit_client(models_index, base_host):
    with open("tests/resources/cbir_fashion_cli_test.jpeg", "rb") as f:
        image_bin = f.read()

    requirements = Requirements(GrpcClientFactory(base_host))
    async with VFlowsContextManager(models_index, requirements) as _cm:
        tagit_client = requirements.clients["210301-cbir-fashion"]

        input = Input(
            entities=[
                InputEntity(
                    listing=[],
                    mapping={},
                    data=image_bin,
                    options=b'{"bounds": [[0.3545147180557251, 0.4682011902332306, 0.6852460503578186, 0.9322336316108704]]}',
                )
                # shold be
                # , InputEntity(
                #     data=b'{"bounds": [[0.3545147180557251, 0.4682011902332306, 0.6852460503578186, 0.9322336316108704]]}'
                # ),
            ],
            options=b"",
        )
        prediction = await tagit_client.predict(input)

        result = DtConverter.to_cbir_result(prediction)
        print("cbir result: ", result)

        assert set(result.keys()) == {
            "category",
            "signatureVector",
            "colorVector",
            "deeptags",
            "similarityGroup",
        }
