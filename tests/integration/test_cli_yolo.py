import json
import pytest
import requests

from vflows.context_manager import VFlowsContextManager
from vflows.requirements import Requirements

from sytedt import inferer as dt

from sytegrpc.client_factory import GrpcClientFactory

IMG_URL_RED_DRESS = (
    "https://i.pinimg.com/736x/23/92/a4/2392a44f930f0e1046fd7084a817d4fd.jpg"
)
IMG_URL_LIVING_ROOM = "https://m.media-amazon.com/images/I/61JLG9VEm8S._SL1173_.jpg"

IMG_COUPLE_ON_KITCHEN = (
    "https://image.shutterstock.com/shutterstock/photos/674419903/display_1500/"
    "stock-photo-secret-ingredient-is-love-beautiful-young-couple-preparing-a-healthy-meal-"
    "together-while-spending-674419903.jpg"
)


def _get_image(image_url):
    return requests.get(image_url).content


@pytest.mark.asyncio
async def test_yolo_flow_client(models_index, base_host):
    requirements = Requirements(GrpcClientFactory(base_host))
    async with VFlowsContextManager(models_index, requirements) as _cm:
        yolo_home_client = requirements.clients["210901-yolo-home"]
        image_home = _get_image(IMG_URL_LIVING_ROOM)

        dt_input = dt.Input(entities=[dt.InputEntity(data=image_home)])
        prediction_inferer = await yolo_home_client.predict(dt_input)
        boundaries_inferer = [
            json.loads(entity.data) for entity in prediction_inferer.entities[0].listing
        ]
        assert len(boundaries_inferer) != 0
