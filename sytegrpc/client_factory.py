from sytedt.client import InferClientFactory
from .flow_client import GrpcFlowClient


class GrpcClientFactory(InferClientFactory):
    def __init__(self, host: str):
        self._host: str = host

    def get_client(self, network: str):
        return GrpcFlowClient(self.__get_host(network))

    def __get_host(self, prefix: str) -> str:
        return self._host.format(prefix)
