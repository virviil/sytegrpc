import orjson
from typing import Any, Dict, Optional, Tuple
import grpc
import grpc.aio
import backoff

from sytepb.inferer_pb2_grpc import InferenceStub
import sytepb.inferer_pb2 as pb
from sytedt import inferer as dt, grpc as dt_grpc
from sytedt.client import InferClient

DEFAULT_TIMEOUT_CONFIG = 10


class GrpcFlowClient(InferClient):
    def __init__(
        self,
        host: str,
        request_args: Dict[str, Any] = None,
        max_tries: int = 1,
        max_time: int = 5,
        timeout: int = DEFAULT_TIMEOUT_CONFIG,
    ):
        self._host = host
        self._request_args: Dict[str, Any] = {"timeout": timeout}

        if request_args:
            self._request_args.update(request_args)

        def not_retriable_error(ex):
            if grpc.StatusCode.INTERNAL == ex.code():
                return True
            if grpc.StatusCode.UNKNOWN == ex.code():
                return True
            # Possibly here will be more cases

            return False

        # Wrapping main method with backoff
        self.predict = backoff.on_exception(  # type: ignore
            backoff.fibo,
            (grpc.RpcError),
            max_tries=max_tries,
            max_time=max_time,
            giveup=not_retriable_error,
        )(self.predict)

    async def predict(
        self,
        input: dt.Input,
        **kvargs,
        # input: dt.Input,
        # model: str = None,
        # flow: str = None,
        # options: dict = None,
        # request_args: dict = None,
    ) -> Tuple[dt.Prediction, dict]:
        """
        Args:
            model: Model name
            flow: Flow name
            options: Options for the flow
            request_args: Request args for the flow
        """
        model: Optional[str] = kvargs.get("model", None)
        flow: Optional[str] = kvargs.get("flow", None)
        options: Optional[dict] = kvargs.get("options", None)
        request_args: Optional[dict] = kvargs.get("request_args", None)
        response: pb.InferResponse = await self._stub_inferer.Infer(
            pb.InferRequest(
                model_name=flow,
                model_version=model,
                input=dt_grpc.Input.to_pb(input),
                options=orjson.dumps(options),
            ),
            **{**self._request_args, **(request_args or {})},
        )
        return (
            dt_grpc.Prediction.from_pb(response.prediction),
            orjson.loads(response.details),
        )

    async def __aenter__(self):
        self._channel = await grpc.aio.secure_channel(
            self._host, grpc.ssl_channel_credentials()
        ).__aenter__()
        self._stub_inferer = InferenceStub(self._channel)
        self._async = True
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self._channel.__aexit__(exc_type, exc_val, exc_tb)
        self._stub_inferer = None
        self._channel = None
        self._async = None
